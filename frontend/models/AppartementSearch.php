<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Appartement;
use yii\db\Query;

/**
 * AppartementSearch represents the model behind the search form about `app\models\Appartement`.
 */
class AppartementSearch extends Appartement
{
    public $complexName;
    public $buildName;
    public $city;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'size', 'priceM', 'priceAll', 'build_id', 'complex_id'], 'integer'],
            [['complexName', 'buildName', 'rooms', 'city'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $subQuery = (new Query())
            ->select(['build_id', 'rooms', 'min(priceAll) as minpriceAll'])
            ->from('appartement')
            ->groupBy(['build_id', 'rooms']);
        $query = (new Query())
            ->from(['u' => $subQuery])
            ->innerJoin(['app' => 'appartement'], 'app.build_id = u.build_id and app.priceAll = u.minpriceAll and app.rooms = u.rooms')
            ->leftJoin('complex', 'complex.id = app.complex_id')
            ->leftJoin('build', 'build.id = app.build_id');
        /*$query = Appartement::find()
            ->innerJoin(['app' => 'appartement'], 'app.build_id = build_id and app.priceAll = minpriceAll and app.rooms = rooms')
            ->groupBy(['build_id', 'rooms']);*/

        /*$appart = $query->all();
        $countApp = count($query->all());
        $deleteQ = [];

        for ($i = 0; $i < $countApp; $i++) {
            for ($j = 0, $p = 0; $j < $countApp; $j++) {
                if (($appart[$i]->build_id == $appart[$j]->build_id) && ($appart[$i]->rooms == $appart[$j]->rooms) && ($appart[$i]->priceAll > $appart[$j]->priceAll)) {
                    $deleteQ[$p] = $i;
                    $p++;
                }
            }
        }
        foreach ($deleteQ as $key) {
            unset($appart[$key]);
        }*/
        /*$dataProvider = new ArrayDataProvider([
            'key' => 'id',
            'allModels' => $appart,
        ]);*/

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        /*$query->leftJoin('complex', 'complex.id = app.complex_id');
        $query->leftJoin('build', 'build.id = app.build_id');*/

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'complexName' => [
                    'asc' => ['complex.complexName' => SORT_ASC],
                    'desc' => ['complex.complexName' => SORT_DESC],
                    'label' => 'Complex',
                ],
                'buildName' => [
                    'asc' => ['build.buildName' => SORT_ASC],
                    'desc' => ['build.buildName' => SORT_DESC],
                    'label' => 'Name',
                ],
                'rooms' => [
                    'asc' => ['app.rooms' => SORT_ASC],
                    'desc' => ['app.rooms' => SORT_DESC],
                ],
                'size' => [
                    'numeric' => ['size' => SORT_NUMERIC],
                ],
                'priceM' => [
                    'numeric' => ['priceM' => SORT_NUMERIC],
                ],
                'priceAll' => [
                    'numeric' => ['priceAll' => SORT_NUMERIC],
                ],
                'city' => [
                    'asc' => ['city' => SORT_ASC],
                    'desc' => ['city' => SORT_DESC],
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'size' => $this->size,
            'priceM' => $this->priceM,
            'priceAll' => $this->priceAll,
            'build_id' => $this->build_id,
            'complex_id' => $this->complex_id,
        ]);

        $query->andFilterWhere(['like', 'complex_id', $this->complex_id])
            ->andFilterWhere(['like', 'build_id', $this->build_id])
            ->andFilterWhere(['like', 'app.rooms', $this->rooms]);

        if ($this->complexName) {
            $query->andFilterWhere(['like', 'complex.complexName', $this->complexName]);
        }
        if ($this->city) {
            $query->andFilterWhere(['like', 'complex.city', $this->city]);
        }
        if ($this->buildName) {
            $query->andFilterWhere(['like', 'build.buildName', $this->buildName]);
        }
        return $dataProvider;
    }
}
