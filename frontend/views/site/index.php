<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\AppartementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Appartements';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="appartement-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>


    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'complexName',
            'city',
            'buildName',
            'size',
            'priceAll'
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
