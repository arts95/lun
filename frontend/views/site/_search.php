<?php
/**
 * @author: Arsenii
 * Date: 26.07.2016
 * Time: 12:06
 */
/* @var $this yii\web\View */
/* @var $model backend\models\AppartementSearch */
/* @var $form yii\widgets\ActiveForm */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="appartement-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'rooms')->dropDownList(['' => 'all', 'studio' => 'studio', '1 room' => '1 rooms', '2 rooms' => '2 rooms', '3 rooms' => '3 rooms', '4 rooms' => '4 rooms', '5 rooms' => '5 rooms', '5 rooms two-storey' => '5 rooms two-storey', '6 rooms two-storey' => '6 rooms two-storey']) ?>

    <?= $form->field($model, 'city') ?>


    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
