<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Complex */

$this->title = 'Update Complex: ' . $model->complexName;
$this->params['breadcrumbs'][] = ['label' => 'Complexes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->complexName, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="complex-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
