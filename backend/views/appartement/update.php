<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Appartement */

$this->title = 'Update Appartement: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Appartements', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="appartement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
