<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Complex;
use backend\models\Build;

/* @var $this yii\web\View */
/* @var $model backend\models\Appartement */
/* @var $form yii\widgets\ActiveForm */
$js = <<<JS
function changePrice() {

    $("#priceM")
    .keyup(function() {
    var value = $( this ).val()*($('#size').val());
    $( "#priceAll" ).val( value );
    })
    .keyup();
    $("#priceAll")
    .keyup(function() {
    var value = $( this ).val()/($('#size').val());
    $( "#priceM" ).val( value );
    })
    .keyup();
    if (isNaN($( "#priceM" ).val())){
        $( "#priceAll" ).val('');
        $( "#priceM" ).val('');
    }
}
$('#size').keyup(changePrice()).keyup();
$('#allBuilds').click(function() {
    if ($('#allBuilds').prop("checked"))
        $('.builds:input:checkbox').attr('checked', true);
    else
        $('.builds:input:checkbox').attr('checked', false);
});
JS;
$this->registerJs($js);
?>

<div class="appartement-form">

    <?php $form = ActiveForm::begin([
        'id' => 'appart_form'
    ]); ?>

    <?= $form->field($model, 'rooms')->dropDownList(['studio' => 'studio', '1 room' => '1 rooms', '2 rooms' => '2 rooms', '3 rooms' => '3 rooms', '4 rooms' => '4 rooms', '5 rooms' => '5 rooms', '5 rooms two-storey' => '5 rooms two-storey', '6 rooms two-storey' => '6 rooms two-storey']) ?>

    <?= $form->field($model, 'size')->textInput(['id' => 'size']) ?>

    <?= $form->field($model, 'priceM')->textInput(['id' => 'priceM']) ?>

    <?= $form->field($model, 'priceAll')->textInput(['id' => 'priceAll']) ?>

    <?php if ($model->isNewRecord) { ?>
    <?= $form->field($model, 'complex_id')->dropDownList(
        ArrayHelper::map(Complex::find()->all(), 'id', 'complexName'),
        [
            'prompt' => 'Select Complex',
            'onchange' => '$.post("index.php?r=appartement/lists&id=' . '"+ $(this).val(), function(data){$("div#appartement-build_id").html(data);});'
        ]) ?>
        <label for="allBuilds"><input type="checkbox" id="allBuilds" > All builds</label>
    <?= $form->field($model, 'build_id')->checkboxList(
        ArrayHelper::map(Build::find()->all(), 'id', 'buildName'),
        [
            'prompt' => 'Select build',
        ]) ?>

    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <p></p>
</div>
