<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Appartement */

$this->title = 'Create Appartement';
$this->params['breadcrumbs'][] = ['label' => 'Appartements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appartement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
