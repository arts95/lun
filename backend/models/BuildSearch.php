<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BuildSearch represents the model behind the search form about `backend\models\Build`.
 */
class BuildSearch extends Build
{
    public $address;
    public $complexName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['buildName', 'street', 'number', 'address', 'complexName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Build::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['complex']);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'address' => [
                    'asc' => ['street' => SORT_ASC, 'number' => SORT_NUMERIC],
                    'desc' => ['street' => SORT_DESC, 'number' => SORT_NUMERIC],
                    'label' => 'Address',
                    'default' => SORT_ASC,
                ],
                'complexName' => [
                    'asc' => ['complex.complexName' => SORT_ASC],
                    'desc' => ['complex.complexName' => SORT_DESC],
                    'label' => 'Complex',
                ],
                'buildName' => [
                    'asc' => ['buildName' => SORT_ASC],
                    'desc' => ['buildName' => SORT_DESC],
                    'label' => 'Name',
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'complex_id' => $this->complex_id,
        ]);

        $query->andFilterWhere(['like', 'buildName', $this->buildName])
            ->andFilterWhere(['like', 'street', $this->street])
            ->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'CONCAT(street, " ", number)', $this->address])
            ->andFilterWhere(['like', 'complex_id', $this->complex_id]);

        if ($this->complexName) {
            $query->andFilterWhere(['like', 'complex.complexName', $this->complexName]);
        }

        return $dataProvider;
    }
}
