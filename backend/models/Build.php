<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "build".
 *
 * @property integer $id
 * @property string $buildName
 * @property string $street
 * @property string $number
 * @property integer $complex_id
 * @property Complex $complex
 * @property string $address
 * @property string $complexName
 */
class Build extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'build';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['buildName', 'street', 'number', 'complex_id'], 'required'],
            [['complex_id', 'number'], 'integer'],
            [['buildName', 'street'], 'string', 'max' => 20],
            [['buildName'], 'unique'],
            [['number', 'street'], 'validateAddress'],
        ];
    }

    public function getComplexes()
    {
        return Complex::findAll(['id' => $this->complex_id]);
    }
    public function validateAddress($attribute, $params)
    {
        $builds = Build::findAll(['street' => $this->street, 'complex_id' => $this->complex_id]);
        if ($builds) {
            foreach ($builds as $build)
                if ($build->id != $this->id)
                    if ($build->number == $this->number) {
                        $this->addError($attribute, 'Address already exists');
                    }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'buildName' => 'Build',
            'street' => 'Street',
            'number' => '# of street',
            'complex_id' => 'Complex',
            'complexName' => 'Complex',
            'address' => 'Address'
        ];
    }

    public function getComplex()
    {
        return $this->hasOne(Complex::className(), ['id' => 'complex_id']);
    }

    public function getComplexName()
    {
        return $this->complex->complexName;
    }

    public function getAddress()
    {
        return $this->street . ' ' . $this->number;
    }
}
