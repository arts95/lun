<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Appartement;
use yii\data\ArrayDataProvider;
use yii\db\Query;

/**
 * AppartementSearch represents the model behind the search form about `app\models\Appartement`.
 */
class AppartementSearch extends Appartement
{
    public $complexName;
    public $buildName;
    public $city;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'size', 'priceM', 'priceAll', 'build_id', 'complex_id'], 'integer'],
            [['complexName', 'buildName', 'rooms', 'city'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Appartement::find();


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['complex', 'build']);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'complexName' => [
                    'asc' => ['complex.complexName' => SORT_ASC],
                    'desc' => ['complex.complexName' => SORT_DESC],
                    'label' => 'Complex',
                ],
                'buildName' => [
                    'asc' => ['build.buildName' => SORT_ASC],
                    'desc' => ['build.buildName' => SORT_DESC],
                    'label' => 'Name',
                ],
                'rooms' => [
                    'asc' => ['rooms' => SORT_ASC],
                    'desc' => ['rooms' => SORT_DESC],
                ],
                'size' => [
                    'numeric' => ['size' => SORT_NUMERIC],
                ],
                'priceM' => [
                    'numeric' => ['priceM' => SORT_NUMERIC],
                ],
                'priceAll' => [
                    'numeric' => ['priceAll' => SORT_NUMERIC],
                ],
                'city' => [
                    'asc' => ['city' => SORT_ASC],
                    'desc' => ['city' => SORT_DESC],
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'size' => $this->size,
            'priceM' => $this->priceM,
            'priceAll' => $this->priceAll,
            'build_id' => $this->build_id,
            'complex_id' => $this->complex_id,
        ]);

        $query->andFilterWhere(['like', 'complex_id', $this->complex_id])
            ->andFilterWhere(['like', 'build_id', $this->build_id])
            ->andFilterWhere(['like', 'rooms', $this->rooms]);

        if ($this->complexName) {
            $query->andFilterWhere(['like', 'complex.complexName', $this->complexName]);
        }
        if ($this->city) {
            $query->andFilterWhere(['like', 'complex.city', $this->city]);
        }
        if ($this->buildName) {
            $query->andFilterWhere(['like', 'build.buildName', $this->buildName]);
        }
        return $dataProvider;
    }
}
