<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "appartement".
 *
 * @property integer $id
 * @property string $rooms
 * @property integer $size
 * @property integer $priceM
 * @property integer $priceAll
 * @property array|integer $build_id
 * @property integer $complex_id
 * @property Complex $complex
 * @property Build $build
 * @property string $complexName
 * @property string $buildName
 * @property string $city
 */
class Appartement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'appartement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rooms', 'size', 'priceM', 'priceAll', 'complex_id'], 'required'],
            [['complex_id'], 'integer'],
            [['size', 'priceM', 'priceAll'], 'double'],
            [['rooms'], 'string'],
            ['build_id', 'safe'],
        ];
    }

    public function saveAppartement()
    {
        if (!$this->validate()) {
            return null;
        }
        $buildsId = $this->build_id;
        foreach ($buildsId as $buildId) {
            $appartement = new Appartement();
            $appartement->rooms = $this->rooms;
            $appartement->size = $this->size;
            $appartement->priceM = $this->priceM;
            $appartement->priceAll = $this->priceAll;
            $appartement->complex_id = $this->complex_id;
            $appartement->build_id = $buildId;
            $appartement->save();
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rooms' => 'Rooms',
            'size' => 'Size M.SQ.',
            'priceM' => 'Price M.SQ.',
            'priceAll' => 'Price for app.',
            'build_id' => 'Build',
            'complex_id' => 'Complex',
            'complexName' => 'Complex',
            'buildName' => 'Build',
            'city' => 'City'
        ];
    }

    public function getComplex()
    {
        return $this->hasOne(Complex::className(), ['id' => 'complex_id']);
    }

    public function getComplexName()
    {
        return $this->complex->complexName;
    }

    public function getCity()
    {
        return $this->complex->city;
    }

    public function getBuild()
    {
        return $this->hasOne(Build::className(), ['id' => 'build_id']);
    }

    public function getBuildName()
    {
        return $this->build->buildName;
    }
}
