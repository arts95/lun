<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "complex".
 *
 * @property integer $id
 * @property string $complexName
 * @property string $city
 */
class Complex extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'complex';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['complexName', 'city'], 'required'],
            [['complexName', 'city'], 'string', 'max' => 40],
            [['complexName'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'complexName' => 'Complex',
            'city' => 'City'
        ];
    }
}
