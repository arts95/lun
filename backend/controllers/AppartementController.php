<?php

namespace backend\controllers;

use backend\models\Build;
use backend\models\BuildSearch;
use backend\models\ComplexSearch;
use Yii;
use backend\models\Appartement;
use backend\models\AppartementSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AppartementController implements the CRUD actions for Appartement model.
 */
class AppartementController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Appartement models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AppartementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Appartement model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Appartement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Appartement();
        if ($model->load(Yii::$app->request->post()) && $model->saveAppartement()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionLists($id)
    {
        $countBuilds = Build::find()
            ->where(['complex_id' => $id])
            ->count();
        $builds = Build::find()
            ->where(['complex_id' => $id])
            ->all();
        if ($countBuilds > 0) {
            foreach ($builds as $build) {
                echo "<label><input name='Appartement[build_id][]' type='checkbox' class='builds' value='" . $build->id . "'>" . ' ' . $build->buildName . "</label><br>";
            }
        } else {
            echo "<option> - </option>";
        }
    }


    /**
     * Updates an existing Appartement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Appartement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Appartement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Appartement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Appartement::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
